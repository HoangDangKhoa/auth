@extends('layouts.master')

@section('content')
<h1>Product</h1>
<div>
    <button class="btn btn-success"onclick="location.href='product/create'">New Product</button>
</div>
<br>
<table class="table">
  <tr>
    <th colspan="5">Procduct</th>
  </tr>
  <tr>
    <th>Name</th>
    <th>Category</th>
    <th>Company</th>
    <th colspan="2">Action</th>
  </tr>
  @foreach ($products as $item)
    <tr>
      <td><a href="/product/{{$item->id}}">{{ $item->name }}</a></td>
      <td>{{ $item->category_id == 1 ? 'Smart Phone' : 'Laptop' }}</td>
      <td>{{ $item->company }}</td>
      <td>
        <button  class="btn btn-warning" onclick="location.href='product/{{$item->id}}/edit'">Edit</button>
      </td>
      <td>
        <form action="product/{{$item->id}}" method="POST">
          @method('DELETE')
          @csrf
          <input hidden name="id" value="{{$item->id }} ">
          <button class="btn btn-outline-danger">Delete</button>
        </form>
      </td>
    </tr>
  @endforeach
</table>


@endsection