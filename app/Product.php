<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaeyKey = 'id';
    protected $fillable = ['name', 'company', 'category_id'];
}
